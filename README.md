# yaZ-DNS - "Yet Another Zig DNS" library

A DNS library in accordance with [RFC
1035](https://www.rfc-editor.org/rfc/rfc1035.html).

I started writing this library to 1) learn more about standards for domain
names, and 2) have a little fun experimenting with the Zig programming language.
It was originally supposed to be a recreation of Emil Hernvall's
[dnsguide](https://github.com/EmilHernvall/dnsguide) in Zig rather than Rust,
however I've decided to fully immerse myself in DNS standards instead. My
reasoning was simple: imitating Rust with Zig is neither useful nor
exciting. Thus, this project represents a challenge to myself to grok DNS at a
far more granular level while learning a new programming language.

Obligatory warning: this is a personal project in the very early stages, and
whether or not the program even compiles could change with every commit. If you
need a more complete library I suggest taking a look at
[zig-dns](https://github.com/dantecatalfamo/zig-dns) or
[zigdig](https://github.com/lun-4/zigdig).
