const std = @import("std");

pub fn main() !void {
    var dir = try std.fs.cwd().openDir("../../test", .{});
    defer dir.close();

    const file = try dir.openFile("response.txt", .{});
    defer file.close();
}
